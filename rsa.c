#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define element_size long long int

#define DEBUG_PRINT 0
#define BITS_REQUIRED (sizeof(value_to_convert) * 8)
#define ELEMENTS_REQUIRED (strlen(binary_array) * sizeof(element_size))
#define ENCRYPTED_DATA (sizeof(c) / sizeof(element_size))

element_size* prime_number(element_size);
element_size extended_euclidean(element_size*, element_size);
char* dec_to_binary (element_size);
element_size* modular_exponentiation(element_size, element_size, char*);
element_size comparison_of_arrays(element_size*, char*, element_size);

void test_operation(element_size*, element_size*, element_size*);

/*------------------------------------------------------------------
prime_number
------------------------------------------------------------------
Factorization of n in 2 prime numbers

Params: long long int n         -> value to factorize (n)

Return: long long int* result   -> value of 2 prime numbers (p, q)
------------------------------------------------------------------*/
element_size* prime_number(element_size n){
    #if DEBUG_PRINT
    printf("-- prime_number --\n");
    #endif
    element_size* result = malloc(sizeof(element_size) * 2);

    int results_index = 0;

    if(n || !(n < 2)){
        for(element_size i = 2; i <= n; i++){
            while(n % i ==0){
                result[results_index] = i;
                n /= i;
                results_index ++;
            }
        }
    }
    #if DEBUG_PRINT
    printf(" [%lld] : [%lld]\n", result[0], result[1]);
    #endif

    return result;
}

/*------------------------------------------------------------------
extended_euclidean
------------------------------------------------------------------
For r[0] = pgcd((p-1)*(q-1) ,b) = (p-1)*(q-1)*x[0] + b*y[0]

Params: long long int* a        -> for n = p*q : a = (p-1)*(q-1)
        long long int b         -> value of e
    
Return: long long int y[0]      -> value of d
------------------------------------------------------------------*/
element_size extended_euclidean(element_size* a, element_size b){
    #if DEBUG_PRINT
    printf("\n--extended_euclidean--\n");
    #endif
    element_size r[3], x[3], y[3];
    element_size temp = (a[0] - 1)*(a[1] - 1);
    element_size result;
    int q;

    // Allocation
    r[0] = temp;    r[1] = b;
    x[0] = 1;       x[1] = 0;
    y[0] = 0;       y[1] = 1;

    #if DEBUG_PRINT
    printf(" q = %d : r = %lld : x = %lld : y = %lld : r' = %lld : x' = %lld : y' = %lld\n", 
    q, r[0], x[0], y[0], r[1], x[1], y[1]);
    #endif

    while(r[1] != 0){
        q = r[0]/r[1];

        r[2] = r[0]; 
        r[0] = r[1];
        r[1] = r[2]-q*r[1]; 
        
        x[2] = x[0]; 
        x[0] = x[1]; 
        x[1] = x[2]-q*x[1]; 
        
        y[2] = y[0];
        y[0] = y[1];
        y[1] = y[2]-q*y[1]; 

        #if DEBUG_PRINT
        printf(" q = %d : r = %lld : x = %lld : y = %lld : r' = %lld : x' = %lld : y' = %lld\n", 
        q, r[0], x[0], y[0], r[1], x[1], y[1]);
        #endif
    }

    if(y[0] < 0){
        y[0] = ((element_size)(-y[0]/temp)+1)*temp + y[0];
    }
    return y[0];
}

/*------------------------------------------------------------------
dec_to_binary
------------------------------------------------------------------
Convert an n from decimal system (base-10) to binary number system (base-2)

Params: long long int value_to_convert  -> value to convert

Return: char* result                    -> n in binary number system
------------------------------------------------------------------*/
char* dec_to_binary(element_size value_to_convert){
    #if DEBUG_PRINT
    printf("\n-- dec_to_binary --\n");
    #endif

    char* result = malloc(BITS_REQUIRED +1);
    int bit_counter;
    int current_bit;

    for(bit_counter = BITS_REQUIRED - 1; bit_counter >= 0; bit_counter--){
        current_bit = value_to_convert >> bit_counter;
        strcat(result, current_bit & 1 ? "1" : "0");
    }
    #if DEBUG_PRINT
    printf(" [%s]", result);
    #endif

    return strchr(result, '1');
}

/*------------------------------------------------------------------
modular_exponentiation
------------------------------------------------------------------
Calculation of the modular inverse

Params: long long int c                 -> value cipher
        long long int m                 -> modulo
        char* binary_array              -> n in binary system
    
Return: long long int result            -> modular inverse of the value cipher
------------------------------------------------------------------*/
element_size* modular_exponentiation(element_size c, element_size m, char* binary_array){
    #if DEBUG_PRINT
    printf("\n\n-- modular_exponentiation --\n");
    #endif

    element_size* result = malloc(ELEMENTS_REQUIRED);

    for(int i =0; i < strlen(binary_array); i++){
        result[i] = (c % m);
        c = (c * c) % m;

        #if DEBUG_PRINT
        printf(" [%02d] : [%lld]\n", i, result[i]);
        #endif
    }
    return result;
}

/*------------------------------------------------------------------
comparison_of_arrays
------------------------------------------------------------------
Comparison between binary and modular arrays

Params: long long int exp_mod_array     -> modular inverse of c
        char* binary_array              -> n in binary system
        long long int m                 -> modulo

Return: long long int result            -> value decipher
------------------------------------------------------------------*/
element_size comparison_of_arrays(element_size* exp_mod_array, char* binary_array, element_size m){
    #if DEBUG_PRINT
    printf("\n-- comparison_of_arrays --\n");
    #endif

    element_size result = 1;
    int current_bit = strlen(binary_array) - 1;
    int current_element = 0;
    
    for(; current_element < strlen(binary_array); current_element++, current_bit--){
        #if DEBUG_PRINT
        printf(" [%c] = [%lld]\n", binary_array[current_bit], exp_mod_array[current_element]);
        #endif

        if(binary_array[current_bit] == '1'){
            result = exp_mod_array[current_element] * result % m;
            if(result < 0){
                result = ((element_size)((-result)/m)+1)*m + result;
                result = result % m;
            }
        }
    }
    return result;
}

/*------------------------------------------------------------------
test_operation
------------------------------------------------------------------
Gives test values to the key
    for (e = 4613; n = 9797; c = 5229)  -> M = 24
    for (e = 17; n = 35; c = 12)        -> M = 17

Params: long long int* e                -> value of e
        long long int* n                -> value of n
        long long int* c                -> value of n
------------------------------------------------------------------*/
void test_operation(element_size* e, element_size* n, element_size* c){
    *e = 17;
    *n = 35;
    *c = 12;

}

/*------------------------------------------------------------------
main
------------------------------------------------------------------*/
int main(void){
    element_size n = 1470213709;
    element_size e = 10321;
    element_size c[2] = {771947451, 46022985};

    /*-- test operation --*/
    //test_operation(&e, &n, &c);


    /*-- find p & q --*/
    element_size *r_prime_number = prime_number(n);
    #if !DEBUG_PRINT
    printf("-> p  = %lld\n-> q  = %lld\n", r_prime_number[0], r_prime_number[1]);
    #endif

    /*-- find d (base 10) & (base 2) --*/
    int r_extended_euclidean = extended_euclidean(r_prime_number, e);
    char* r_dec_to_binary = dec_to_binary(r_extended_euclidean);
    #if !DEBUG_PRINT
    printf("-> d  = %d\n      = %s (base 2)\n", r_extended_euclidean, r_dec_to_binary);
    #endif

    /*-- find M --*/
    for(int i = 0; i < ENCRYPTED_DATA; i++){
        element_size* r_modular_exponentiation = modular_exponentiation(c[i], n, r_dec_to_binary);
        element_size r_comparision_of_arrays = comparison_of_arrays(r_modular_exponentiation, r_dec_to_binary, n);

        #if !DEBUG_PRINT
        printf("-> M%d = %f\n", i + 1, (double)(r_comparision_of_arrays) / 1000000.0);
        #endif
    }
    return 0;
}

